const axios = require("axios");
const cheerio = require("cheerio");
const HttpsProxyAgent = require("https-proxy-agent");
const url = require("url");
const cron = require("node-cron");

const Announcement = require("./models/announcement");
const Proxy = require("./models/proxy");

cron.schedule("*/4 * * * *", () => {
  startParsing();
});

async function startParsing() {
  const totalPages = await getTotalPages();
  for (let i = 0; i < 2; i++) {
    console.log(new Date());

    const randomPage = parseInt(Math.random() * totalPages + 1);

    console.log("random page:", randomPage);

    await parsePage(randomPage);
  }
}

async function parsePage(randomPage) {
  const proxies = await Proxy.find({});
  const proxy = proxies[parseInt(Math.random() * proxies.length)];
  var proxyOpts = url.parse(proxy.proxy);
  agent = new HttpsProxyAgent(proxyOpts);

  let dataFromMainPage = await axios.get(
    "https://krisha.kz/prodazha/kvartiry/" + "/?page=" + randomPage,
    {
      //   httpsAgent: agent,
    }
  );

  await sleep(2000);

  const $ = cheerio.load(dataFromMainPage.data);

  const allCards = $(".a-card");

  allCards.each(async (index, data) => {
    const id = $(data).attr("data-id");

    await parseApartment(id);
  });
}

async function parseApartment(id) {
  const proxies = await Proxy.find({});
  const proxy = proxies[parseInt(Math.random() * proxies.length)];
  var proxyOpts = url.parse(proxy.proxy);
  agent = new HttpsProxyAgent(proxyOpts);

  const link = "https://krisha.kz/a/show/" + id;

  const apartmentPage = await axios.get(link, {
    // httpsAgent: agent,
  });
  await sleep(2000);

  const $ = cheerio.load(apartmentPage.data);

  const offerInfo = $(".offer__info-item");

  let title = $(".offer__advert-title > h1").text().trim();

  let city = "";
  let district = "";
  let typeHome = "";
  let year = 0;
  let area = 0.0;
  let residentialComplex = "";
  let condition = "";
  let apartment = parseInt(title.split(",")[0].split("-")[0]);
  let floor = 1;
  let price = parseInt($(".offer__price").text().trim().replace(/[^\d]/g, ""));
  offerInfo.each((index, data) => {
    let infoItem = $(data).text().trim();

    if (infoItem.includes("Город")) {
      try {
        infoItem = infoItem
          .replace("Город", "")
          .replace("показать на карте", "")
          .trim();
        city = infoItem.split(",")[0].trim();
        district = infoItem.split(",")[1].trim();
      } catch (e) {}
    } else if (infoItem.includes("Дом")) {
      try {
        infoItem = infoItem.replace("Дом", "").trim();
        typeHome = infoItem.split(",")[0].trim();
        year = parseInt(infoItem.split(",")[1].replace("г.п.", "").trim());
      } catch (e) {}
    } else if (infoItem.includes("Площадь")) {
      infoItem = infoItem.replace("Площадь", "").replace("м²", "").trim();
      area = parseFloat(infoItem);
    } else if (infoItem.includes("Жилой комплекс")) {
      residentialComplex = infoItem.replace("Жилой комплекс", "").trim();
    } else if (infoItem.includes("Состояние")) {
      condition = infoItem.replace("Состояние", "").trim();
    } else if (infoItem.includes("Этаж")) {
      infoItem = infoItem.replace("Этаж", "").trim();
      floor = parseInt(infoItem.split("из")[0]);
    }
  });

  const foundAnnouncement = await Announcement.findOne({ id });

  if (!foundAnnouncement) {
    await new Announcement({
      id,
      title,
      city,
      district,
      typeHome,
      year,
      area,
      residentialComplex,
      condition,
      title,
      apartment,
      floor,
      price,
      link,
    }).save();

    console.log("saved:", title);
  } else {
    console.log("exists in db");
  }
}

async function getTotalPages() {
  const proxies = await Proxy.find({});
  const proxy = proxies[parseInt(Math.random() * proxies.length)];
  var proxyOpts = url.parse(proxy.proxy);
  agent = new HttpsProxyAgent(proxyOpts);

  const mainPage = await axios.get(
    "https://krisha.kz/prodazha/kvartiry/?page=1",
    {
      //   httpsAgent: agent,
    }
  );

  const $ = cheerio.load(mainPage.data);

  const buttons = $(".paginator__btn");

  let maxPage = 1;
  buttons.each((index, button) => {
    try {
      const textBtn = $(button).text().replace(/ /g, "").replace(/\/n/g, "");
      const page = parseInt(textBtn);

      if (!Number.isNaN(page)) {
        maxPage = page;
      }
    } catch (e) {}
  });

  return maxPage;
}

const sleep = (ms) => {
  return new Promise((resolve) => setTimeout(resolve, ms));
};

module.exports = startParsing;
