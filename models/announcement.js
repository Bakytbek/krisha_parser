const { Schema, model } = require("mongoose");

const announcementSchema = new Schema(
  {
    id: {
      type: Number,
      required: true,
    },
    title: {
      type: String,
      required: true,
    },
    apartment: {
      // сколько комнат
      type: Number,
      required: true,
    },
    price: {
      type: Number,
      required: true,
    },
    city: {
      type: String,
      required: true,
    },
    district: {
      // район
      type: String,
      default: "",
    },
    typeHome: {
      type: String,
      required: true,
    },
    floor: {
      // Этаж
      type: Number,
      required: true,
    },
    area: {
      // площадь
      type: Number,
      required: true,
    },
    condition: {
      // состояние
      type: String,
      default: "",
    },
    residentialComplex: {
      // жилой комплекс
      type: String,
      default: "",
    },
    year: {
      type: Number,
      required: true,
    },
    link: {
      type: String,
      required: true,
    },
  },
  {
    timestamps: true,
  }
);

module.exports = model("announcement", announcementSchema);
