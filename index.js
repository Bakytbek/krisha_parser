const mongoose = require("mongoose");
const config = require("config");

// const startParsing = require("./startParsing");

mongoose.connect(config.get("db"), async () => {
  // await startParsing();
  require("./startParsing");
});
